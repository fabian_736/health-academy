<nav class="navbar navbar-expand-lg navbar-transparent" style="padding-left: 5%; padding-right: 5%">

    <a class="navbar-brand" href="{{route('portal_patient.index')}}">
        <img src="{{url('img/logo_login.png')}}" class="d-inline-block align-top img-responsive " alt="">
    </a>

    <div class="collapse navbar-collapse mt-5" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto ml-5">
            <li class="nav-item active">
                <form class="navbar-form">
                    <div class="input-group no-border">
                        <input type="text" value="" class="form-control" placeholder="Search...">
                        <button type="submit" class="btn btn-white btn-round btn-just-icon">
                            <i class="material-icons">search</i>
                            <div class="ripple-container"></div>
                        </button>
                    </div>
                </form>
            </li>
        </ul>
    </div>

</nav>