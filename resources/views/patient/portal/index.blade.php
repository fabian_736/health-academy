@extends('layouts.patient.app')
@section('content')

<div class="row">
    <div class="col mb-5">
        <div class="row">
            <div class="col-md-2 p-0 d-flex justify-content-end align-items-center">
                <div class="fileinput fileinput-new text-center m-0" data-provides="fileinput">
                    <div class="fileinput-new thumbnail img-circle img-raised p-1 bg-white " style="width: 200px; height: 100px" >
                        <a href="{{route('profile_patient.index')}}"><img src="{{ url('img/user2.png') }}" style="border-radius: 60px; width: 100%; height: 100%" rel="nofollow" alt="..."></a>
                    </div>
                </div>
            </div>
            <div class="col-md-3 p-0 m-0">
                <div class="row-reverse p-0 m-0">
                    <div class="col m-0 p-0">
                        <label for="" class="h3 font-weight-bold" style="float: right; color: #38B9C3">2000 puntos</label>
                    </div>
                    <div class="col m-0 p-0">
                        <hr class="m-0 p-0" style="width: 100%; height: 2px; border-style: solid; border-color: blue; float: left">
                    </div>
                    <div class="col m-0 p-0">
                        <label for="" class="font-weight-bold" style="float: right; color: #38B9C3">AGREGAR PROGRESO</label>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</div>
<div class="row-reverse mt-5 pl-4">
    <div class="col">
    <label for="" class="h2 font-weight-bold" style="color: #A0C96D">Cristina Sarin</label>
    </div>
    <div class="col">
    <label for="" class="h3" style="color: #A0C96D">Cardiopatia</label>
    </div>
    <div class="col">
    <label for="" class="h4" style="color: #A0C96D">N. 1234-BC</label>
    </div>
</div>


@endsection