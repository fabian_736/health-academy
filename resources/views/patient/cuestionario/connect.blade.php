<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body > 
<div id="page_connections" style="position: absolute; margin: 0 !important; padding: 0 !important; ">
<div class="row">
                    <div id="select_list_lebensbereiche">
                        <ul class="listado">
                            <li id="match1">Miedo a la soledad</li>
                            <li id="match2">Falta de confianza, desconfianza</li>
                            <li id="match3">Necesidad de aprobación</li>
                        </ul>
                    </div>
                    <div id="select_list_wirkdimensionen">
                        <ul class="listado">
                            <li id="answer1" class="text-right">Aceptación de nuestras cualidades y defectos como eres humanos</li>
                            <li id="answer2" class="text-right">Creer en nuestro valor y documentar las posibilidades</li>
                            <li id="answer3" class="text-right">Búsqueda de una compañía que sea grata y buen uso del tiempo libre</li>
                        </ul>
                    </div>
                    </div>
                </div>

</body>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jsPlumb/2.15.0/js/jsplumb.min.js"></script>

<script>
    jQuery(document).ready(function() {
  var targetOption = {
    anchor: "LeftMiddle",
    maxConnections: 1,
    isSource: false,
    isTarget: true,
    reattach: true,
    endpoint: "Dot",
    connector: [ "Bezier", { curviness: 50 } ],
    setDragAllowedWhenFull: true
  };

  var sourceOption = {
    tolerance: "touch",
    anchor: "RightMiddle",
    maxConnections: 1,
    isSource: true,
    isTarget: false,
    reattach: true,
    endpoint: "Dot",
    connector: [ "Bezier", { curviness: 50 } ],
    setDragAllowedWhenFull: true
  };

  jsPlumb.importDefaults({
    ConnectionsDetachable: true,
    ReattachConnections: true,
    maxConnections: 1,
    Container: "page_connections"
  });

  var questionEndpoints = []; // 'source' and 'target' endpoints

  // "source" click handler
  jQuery("#select_list_lebensbereiche ul > li").click(function() {
    //remove existing start endpoint, if any:
    jsPlumb.deleteEndpoint(questionEndpoints[0]);
   
    // add a new one on the clicked element:
    questionEndpoints[0] = jsPlumb.addEndpoint(jQuery(this), sourceOption);
    connectEndpoints();
  });

  // "target" endpoint
  jQuery("#select_list_wirkdimensionen ul > li").click(function() {
    if (!questionEndpoints[0]) return; // don't respond if a source hasn't been selected
    // remove existing endpoint if any
    jsPlumb.deleteEndpoint(questionEndpoints[1]);
    //create a new one:
    questionEndpoints[1] = jsPlumb.addEndpoint(jQuery(this), targetOption);
    connectEndpoints();
  });

  var connectEndpoints = function() {
    jsPlumb.connect({
      source: questionEndpoints[0],
      target: questionEndpoints[1]
    });
  }
  
});


</script>



<style>
    .listado {
	 padding: 0;
	 margin-left: 150px;

	 float: left;
}
 .listado li {
	 list-style: none;
	 background-color: #ededed;
  border-radius: 20px;
  margin-bottom: 20px;
  padding: 10px;
}

 .jtk-connector path {
	 stroke: #38B9C3;
	 stroke-width: 5;

}
 .jtk-endpoint circle {
	 fill: #38B9C3;
}

 
</style>
</html>