@extends('layouts.professional.app')
@section('content')


    <div class="row p-0 my-3 ">
        <div class="col ">
            <label for="" class="h2 font-weight-bold" style="color: #38B9C3">Premios</label>
        </div>
    </div>

    <div class="row-reverse ">
        <div class="row p-0 m-0">
            <div class="col" >
                <div class="row">
                    <div class="col-md-3 p-0 d-flex justify-content-end align-items-center">
                        <div class="fileinput fileinput-new text-center mt-3" data-provides="fileinput">
                            <div class="fileinput-new thumbnail img-circle img-raised p-1 d-flex justify-content-center align-items-center"
                                id="itemDiv" style="width: 60px; height: 60px;">
                                <a href="javascript:;" onclick="clickOn()"><i class="fas fa-gifts" id="ico"
                                        style="font-size: 30px; color: #38B9C3; "></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 p-0 m-0">
                        <div class="row-reverse p-0 m-0">
                            <div class="col m-0 p-0">
                                <label for="" class="h3 font-weight-bold" style="float: right; color: #38B9C3">Bono</label>
                            </div>
                            <div class="col m-0 p-0">
                                <hr class="m-0 p-0"
                                    style="width: 100%; height: 2px; border-style: solid; border-color: #38B9C3; float: left">
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col">
                <div class="row">
                    <div class="col-md-3 p-0 d-flex justify-content-end align-items-center">
                        <div class="fileinput fileinput-new text-center mt-3" data-provides="fileinput">
                            <div class="fileinput-new thumbnail img-circle img-raised p-1 d-flex justify-content-center align-items-center"
                                id="itemDiv" style="width: 60px; height: 60px;">
                                <a href="javascript:;" onclick="clickOn()"><i class="fas fa-headphones-alt" id="ico"
                                        style="font-size: 30px; color: #38B9C3; "></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 p-0 m-0">
                        <div class="row-reverse p-0 m-0">
                            <div class="col m-0 p-0">
                                <label for="" class="h3 font-weight-bold"
                                    style="float: right; color: #38B9C3">Audifono</label>
                            </div>
                            <div class="col m-0 p-0">
                                <hr class="m-0 p-0"
                                    style="width: 100%; height: 2px; border-style: solid; border-color: #38B9C3; float: left">
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="row ">
            <div class="col" >
                <a href="{{ route('premio_profesional.select') }}">
                    <div class="row p-0 m-0">
                        <div class="col-md-3 p-0 d-flex justify-content-end align-items-center">
                            <div class="fileinput fileinput-new text-center mt-3" data-provides="fileinput">
                                <div class="fileinput-new thumbnail img-circle img-raised p-1 d-flex justify-content-center align-items-center"
                                    id="itemDiv" style="width: 60px; height: 60px;">
                                    <a href="{{ route('premio_profesional.select') }}" onclick="clickOn()"><i
                                            class="fas fa-film" id="ico"
                                            style="font-size: 30px; color: #38B9C3; "></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 p-0 m-0">

                            <div class="row-reverse p-0 m-0">
                                <div class="col m-0 p-0">
                                    <label for="" class="h3 font-weight-bold"
                                        style="float: right; color: #38B9C3">Cine</label>
                                </div>
                                <div class="col m-0 p-0">
                                    <hr class="m-0 p-0"
                                        style="width: 100%; height: 2px; border-style: solid; border-color: #38B9C3; float: left">
                                </div>
                            </div>

                        </div>
                    </div>
                </a>
            </div>
            <div class="col" >
                <div class="row" >
                    <div class="col-md-3 p-0 d-flex justify-content-end align-items-center">
                        <div class="fileinput fileinput-new text-center mt-3" data-provides="fileinput">
                            <div class="fileinput-new thumbnail img-circle img-raised p-1 d-flex justify-content-center align-items-center"
                                id="itemDiv" style="width: 60px; height: 60px;">
                                <a href="javascript:;" onclick="clickOn()"><i class="fas fa-tv" id="ico"
                                        style="font-size: 30px; color: #38B9C3; "></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 p-0 m-0">
                        <div class="row-reverse p-0 m-0">
                            <div class="col m-0 p-0">
                                <label for="" class="h3 font-weight-bold" style="float: right; color: #38B9C3">Curso
                                    virtual</label>
                            </div>
                            <div class="col m-0 p-0">
                                <hr class="m-0 p-0"
                                    style="width: 100%; height: 2px; border-style: solid; border-color: #38B9C3; float: left">
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="row p-0 m-0">
            <div class="col p-0 m-0 ">
                <div class="row mx-auto p-0 m-0 ">
                    <div class="col-md-3 p-0 d-flex justify-content-end align-items-center">
                        <div class="fileinput fileinput-new text-center mt-3" data-provides="fileinput">
                            <div class="fileinput-new thumbnail img-circle img-raised p-1 d-flex justify-content-center align-items-center"
                                id="itemDiv" style="width: 60px; height: 60px;">
                                <a href="javascript:;" onclick="clickOn()"><i class="fas fa-dumbbell" id="ico"
                                        style="font-size: 30px; color: #38B9C3; "></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 p-0 m-0">
                        <div class="row-reverse p-0 m-0">
                            <div class="col m-0 p-0">
                                <label for="" class="h3 font-weight-bold" style="float: right; color: #38B9C3">Gym</label>
                            </div>
                            <div class="col m-0 p-0">
                                <hr class="m-0 p-0"
                                    style="width: 100%; height: 2px; border-style: solid; border-color: #38B9C3; float: left">
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col m-0 p-0">

            </div>
        </div>
    </div>


    

@endsection
