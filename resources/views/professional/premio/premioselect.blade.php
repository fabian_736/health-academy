@extends('layouts.professional.app')
@section('content')


<div class="row p-0 my-3 ">
    <div class="col ">
        <label for="" class="h2" style="color: #38B9C3"><b>Premios</b> - Cine</label>
    </div>
</div>

<!-- LISTADO DE PREMIOS -->
<div class="row-reverse ">
    <div class="row p-0 m-0 mb-3">
        <div class="col">
            <div class="row">
                <div class="col-md-3 p-0 d-flex justify-content-end align-items-center">
                    <div class="list-group fileinput fileinput-new text-center mt-3" data-provides="fileinput">
                        <div class=" fileinput-new thumbnail img-circle img-raised d-flex justify-content-center align-items-center" id="itemDiv" style="width: 55px; height: 55px;">
                            <a href="javascript:;" data-toggle="modal" data-target="#exampleModal" onclick="clickOn()"><i class="fas fa-gifts list-group-item" id="ico" style="font-size: 30px; "></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 p-0 m-0">
                    <div class="row-reverse p-0 m-0">
                        <div class="col m-0 p-0">
                            <label for="" class="h3 font-weight-bold" style="float: right; color: #38B9C3">Cine para 2</label>
                        </div>
                        <div class="col m-0 p-0">
                            <hr class="m-0 p-0" style="width: 100%; height: 2px; border-style: solid; border-color: #38B9C3; float: left">
                        </div>
                        <div class="col m-0 p-0">
                            <label for="" class="lead font-weight-bold p-0 m-0" style="float: right; color: #38B9C3">150 Puntos</label>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="col">
            <div class="row">
                <div class="col-md-3 p-0 d-flex justify-content-end align-items-center">
                    <div class="list-group fileinput fileinput-new text-center mt-3" data-provides="fileinput">
                        <div class=" fileinput-new thumbnail img-circle img-raised d-flex justify-content-center align-items-center" id="itemDiv" style="width: 55px; height: 55px;">
                            <a href="javascript:;" data-toggle="modal" data-target="#exampleModal6"><i class="fas fa-gifts list-group-item" id="ico" style="font-size: 30px; "></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 p-0 m-0">
                    <div class="row-reverse p-0 m-0">
                        <div class="col m-0 p-0">
                            <label for="" class="h3 font-weight-bold" style="float: right; color: #38B9C3">Cine para 3</label>
                        </div>
                        <div class="col m-0 p-0">
                            <hr class="m-0 p-0" style="width: 100%; height: 2px; border-style: solid; border-color: #38B9C3; float: left">
                        </div>
                        <div class="col m-0 p-0">
                            <label for="" class="lead font-weight-bold p-0 m-0" style="float: right; color: #38B9C3">350 Puntos</label>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="row p-0 m-0 mb-3">
        <div class="col">
            <div class="row">
                <div class="col-md-3 p-0 d-flex justify-content-end align-items-center">
                    <div class="list-group fileinput fileinput-new text-center mt-3" data-provides="fileinput">
                        <div class=" fileinput-new thumbnail img-circle img-raised d-flex justify-content-center align-items-center" id="itemDiv" style="width: 55px; height: 55px;">
                            <a href="javascript:;" data-toggle="modal" data-target="#exampleModal3"><i class="fas fa-gifts list-group-item" id="ico" style="font-size: 30px; "></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 p-0 m-0">
                    <div class="row-reverse p-0 m-0">
                        <div class="col m-0 p-0">
                            <label for="" class="h3 font-weight-bold" style="float: right; color: #38B9C3">Combo cine 1</label>
                        </div>
                        <div class="col m-0 p-0">
                            <hr class="m-0 p-0" style="width: 100%; height: 2px; border-style: solid; border-color: #38B9C3; float: left">
                        </div>
                        <div class="col m-0 p-0">
                            <label for="" class="lead font-weight-bold p-0 m-0" style="float: right; color: #38B9C3">150 Puntos</label>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="col">
            <div class="row">
                <div class="col-md-3 p-0 d-flex justify-content-end align-items-center">
                    <div class="list-group fileinput fileinput-new text-center mt-3" data-provides="fileinput">
                        <div class=" fileinput-new thumbnail img-circle img-raised d-flex justify-content-center align-items-center" id="itemDiv" style="width: 55px; height: 55px;">
                            <a href="javascript:;" onclick="clickOn()"><i class="fas fa-gifts list-group-item" id="ico" style="font-size: 30px; "></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 p-0 m-0">
                    <div class="row-reverse p-0 m-0">
                        <div class="col m-0 p-0">
                            <label for="" class="h3 font-weight-bold" style="float: right; color: #38B9C3">Combo cine 2</label>
                        </div>
                        <div class="col m-0 p-0">
                            <hr class="m-0 p-0" style="width: 100%; height: 2px; border-style: solid; border-color: #38B9C3; float: left">
                        </div>
                        <div class="col m-0 p-0">
                            <label for="" class="lead font-weight-bold p-0 m-0" style="float: right; color: #38B9C3">150 Puntos</label>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="row p-0 m-0 mb-3">
        <div class="col">
            <div class="row">
                <div class="col-md-3 p-0 d-flex justify-content-end align-items-center">
                    <div class="list-group fileinput fileinput-new text-center mt-3" data-provides="fileinput">
                        <div class=" fileinput-new thumbnail img-circle img-raised d-flex justify-content-center align-items-center" id="itemDiv" style="width: 55px; height: 55px;">
                            <a href="javascript:;" onclick="clickOn()"><i class="fas fa-gifts list-group-item" id="ico" style="font-size: 30px; "></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 p-0 m-0">
                    <div class="row-reverse p-0 m-0">
                        <div class="col m-0 p-0">
                            <label for="" class="h3 font-weight-bold" style="float: right; color: #38B9C3">Combo cine 3</label>
                        </div>
                        <div class="col m-0 p-0">
                            <hr class="m-0 p-0" style="width: 100%; height: 2px; border-style: solid; border-color: #38B9C3; float: left">
                        </div>
                        <div class="col m-0 p-0">
                            <label for="" class="lead font-weight-bold p-0 m-0" style="float: right; color: #38B9C3">150 Puntos</label>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="col">
            <div class="row">
                <div class="col-md-3 p-0 d-flex justify-content-end align-items-center">
                    <div class="list-group fileinput fileinput-new text-center mt-3" data-provides="fileinput">
                        <div class=" fileinput-new thumbnail img-circle img-raised d-flex justify-content-center align-items-center" id="itemDiv" style="width: 55px; height: 55px;">
                            <a href="javascript:;" onclick="clickOn()"><i class="fas fa-gifts list-group-item" id="ico" style="font-size: 30px; "></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 p-0 m-0">
                    <div class="row-reverse p-0 m-0">
                        <div class="col m-0 p-0">
                            <label for="" class="h3 font-weight-bold" style="float: right; color: #38B9C3">Combo cine 4</label>
                        </div>
                        <div class="col m-0 p-0">
                            <hr class="m-0 p-0" style="width: 100%; height: 2px; border-style: solid; border-color: #38B9C3; float: left">
                        </div>
                        <div class="col m-0 p-0">
                            <label for="" class="lead font-weight-bold p-0 m-0" style="float: right; color: #38B9C3">150 Puntos</label>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<!-- CANJEAR PREMIO DOMICILIO -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

            </div>
            <div class="modal-body m-0 p-0" style="padding-bottom: 25% !important;">
                <div class="row m-0 p-0">
                    <div class="col-md-3 p-0 m-0 d-flex justify-content-end align-items-center">
                        <div class="list-group fileinput fileinput-new text-center mt-3" data-provides="fileinput">
                            <div class="fileinput-new thumbnail img-circle img-raised p-1 bg-white" style="background: radial-gradient(54.06% 54.06% at 18.84% 26.21%, #AEDC5A 0%, #38B9C3 100%); width: 250px; height:100px">
                                <a href="#" data-toggle="modal" data-target="#exampleModal2"><img src="https://i2.wp.com/hipertextual.com/wp-content/uploads/2014/06/15-cine-scaled.jpg?fit=2560%2C1920&ssl=1" style="width: 100%; height: 100%; border-radius: 60px" rel="nofollow" alt="..."></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 p-0 m-0">
                        <div class="row-reverse p-0 m-0">
                            <div class="col m-0 p-0">
                                <label for="" class="h3 font-weight-bold" style="float: right; color: #38B9C3">Cine para 2</label>
                            </div>
                            <div class="col m-0 p-0">
                                <hr class="m-0 p-0" style="width: 100%; height: 2px; border-style: solid; border-color: #38B9C3; float: left">
                            </div>
                            <div class="col m-0 p-0">
                                <label for="" class="lead font-weight-bold p-0 m-0" style="float: right; color: #A0C96D">150 Puntos</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row-reverse p-3">
                    <div class="col">
                        <label for="" class="font-weight-bold">Entrega</label>
                    </div>
                    <div class="col">
                        <label for="">Domicilio incluido</label>
                    </div>
                </div>
                <div class="row-reverse p-3">
                    <div class="col">
                        <label for="" class="h4 font-weight-bold" style="color: #38B9C3">Información del producto</label>
                    </div>
                    <div class="col">
                        <label for="">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis.</label>
                    </div>
                </div>
                <div class="row-reverse p-3">
                    <div class="col">
                        <label for="" class="h4 font-weight-bold" style="color: #38B9C3">Características</label>
                    </div>
                    <div class="col ml-3">
                        <ul class="p-0 m-0">
                            <li>Dos boletas de cinecolombia generales</li>
                            <li>No incluye alimentos</li>
                            <li>Vigencia de tres meses desde su obtención</li>
                        </ul>
                    </div>
                </div>
                <div class="row ">
                    <div class="col d-flex justify-content-end ">
                        <a href="javascript:;" data-toggle="modal" data-target="#exampleModal4">
                            <img src="{{url('img/CanjearPremio.png')}}" alt="" id="image" style="position: absolute; left: 69%; width: 30%; height: 80px">
                            <img src="{{url('img/CanjeadoPremio.png')}}" alt="" id="image2" style="display: none; position: absolute; left: 70%; width: 30%; height: 80px">
                        </a>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- CANJEAR PREMIO ONLINE -->
<div class="modal fade" id="exampleModal6" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

            </div>
            <div class="modal-body m-0 p-0" style="padding-bottom: 25% !important;">
                <div class="row m-0 p-0">
                    <div class="col-md-3 p-0 m-0 d-flex justify-content-end align-items-center">
                        <div class="list-group fileinput fileinput-new text-center mt-3" data-provides="fileinput">
                            <div class="fileinput-new thumbnail img-circle img-raised p-1 bg-white" style="background: radial-gradient(54.06% 54.06% at 18.84% 26.21%, #AEDC5A 0%, #38B9C3 100%); width: 250px; height:100px">
                                <a href="#" data-toggle="modal" data-target="#exampleModal2"><img src="https://i2.wp.com/hipertextual.com/wp-content/uploads/2014/06/15-cine-scaled.jpg?fit=2560%2C1920&ssl=1" style="width: 100%; height: 100%; border-radius: 60px" rel="nofollow" alt="..."></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 p-0 m-0">
                        <div class="row-reverse p-0 m-0">
                            <div class="col m-0 p-0">
                                <label for="" class="h3 font-weight-bold" style="float: right; color: #38B9C3">Cine para 2</label>
                            </div>
                            <div class="col m-0 p-0">
                                <hr class="m-0 p-0" style="width: 100%; height: 2px; border-style: solid; border-color: #38B9C3; float: left">
                            </div>
                            <div class="col m-0 p-0">
                                <label for="" class="lead font-weight-bold p-0 m-0" style="float: right; color: #A0C96D">150 Puntos</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row-reverse p-3">
                    <div class="col">
                        <label for="" class="font-weight-bold">Entrega</label>
                    </div>
                    <div class="col">
                        <label for="">Domicilio incluido</label>
                    </div>
                </div>
                <div class="row-reverse p-3">
                    <div class="col">
                        <label for="" class="h4 font-weight-bold" style="color: #38B9C3">Información del producto</label>
                    </div>
                    <div class="col">
                        <label for="">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis.</label>
                    </div>
                </div>
                <div class="row-reverse p-3">
                    <div class="col">
                        <label for="" class="h4 font-weight-bold" style="color: #38B9C3">Características</label>
                    </div>
                    <div class="col ml-3">
                        <ul class="p-0 m-0">
                            <li>Dos boletas de cinecolombia generales</li>
                            <li>No incluye alimentos</li>
                            <li>Vigencia de tres meses desde su obtención</li>
                        </ul>
                    </div>
                </div>
                <div class="row ">
                    <div class="col d-flex justify-content-end ">
                        <a href="javascript:;" data-toggle="modal" data-target="#exampleModal5">
                            <img src="{{url('img/CanjearPremio.png')}}" alt="" id="imageO1" style="position: absolute; left: 69%; width: 30%; height: 80px">
                            <img src="{{url('img/CanjeadoPremio.png')}}" alt="" id="imageO2" style="display: none; position: absolute; left: 70%; width: 30%; height: 80px">
                        </a>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- PREMIO CANJEADO -->
<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog p-0" style="margin-left: 10%;" role="document">
        <div class="modal-content p-0 m-0">
            <div class="modal-body m-0 p-0 transparent">
                <img data-dismiss="modal" aria-label="Close" class="m-0 p-0" src="https://i2.wp.com/hipertextual.com/wp-content/uploads/2014/06/15-cine-scaled.jpg?fit=2560%2C1920&ssl=1" style="width: 1000px; height: 500px" alt="">
            </div>
        </div>
    </div>
</div>

<!-- NO SE PUEDE CANJEAR -->
<div class="modal fade" id="exampleModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

            </div>
            <div class="modal-body m-0 p-0" style="padding-bottom: 25% !important;">
                <div class="row m-0 p-0">
                    <div class="col-md-3 p-0 m-0 d-flex justify-content-end align-items-center">
                        <div class="list-group fileinput fileinput-new text-center mt-3" data-provides="fileinput">
                            <div class="fileinput-new thumbnail img-circle img-raised p-1 bg-white" style="background: gray; width: 250px; height:100px">
                                <a href="#" data-toggle="modal" data-target="#exampleModal3"><img src="https://i2.wp.com/hipertextual.com/wp-content/uploads/2014/06/15-cine-scaled.jpg?fit=2560%2C1920&ssl=1" style="width: 100%; height: 100%; border-radius: 60px; opacity: 0.5" rel="nofollow" alt="..."></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 p-0 m-0">
                        <div class="row-reverse p-0 m-0">
                            <div class="col m-0 p-0">
                                <label for="" class="h3 font-weight-bold" style="float: right; color: gray">Cine para 2</label>
                            </div>
                            <div class="col m-0 p-0">
                                <hr class="m-0 p-0" style="width: 100%; height: 2px; border-style: solid; border-color: gray; float: left">
                            </div>
                            <div class="col m-0 p-0">
                                <label for="" class="lead font-weight-bold p-0 m-0" style="float: right; color: gray">150 Puntos</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row-reverse p-3">
                    <div class="col">
                        <label for="" class="font-weight-bold">Entrega</label>
                    </div>
                    <div class="col">
                        <label for="">Domicilio incluido</label>
                    </div>
                </div>
                <div class="row-reverse p-3">
                    <div class="col">
                        <label for="" class="h4 font-weight-bold" style="color: gray">Información del producto</label>
                    </div>
                    <div class="col">
                        <label for="">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis.</label>
                    </div>
                </div>
                <div class="row-reverse p-3">
                    <div class="col">
                        <label for="" class="h4 font-weight-bold" style="color: gray">Características</label>
                    </div>
                    <div class="col ml-3">
                        <ul class="p-0 m-0">
                            <li>Dos boletas de cinecolombia generales</li>
                            <li>No incluye alimentos</li>
                            <li>Vigencia de tres meses desde su obtención</li>
                        </ul>
                    </div>
                </div>
                <div class="row ">
                    <div class="col d-flex justify-content-end ">
                        <a href="javascript:;">
                            <img src="{{url('img/NoCanjeadoPremio.png')}}" alt="" style="position: absolute; left: 60%; width: 40%; height: 80px">

                        </a>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- FORM DOMICILIO -->
<div class="modal fade" id="exampleModal4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row-reverse">
                    <div class="col">
                        <label for="" style="color: #38B9C3" class="font-weight-bold lead">CANJEAR PREMIO - FÍSICO</label>
                    </div>
                    <div class="col">
                        <label for="">Por favor registre y confirme los datos para poder canjear y entregar efectivamente tu premio:</label>
                    </div>
                </div>
                <div class="row-reverse my-3">
                    <div class="col">
                        <input type="text" class="form-control" placeholder="NOMBRES Y APELLIDOS">
                    </div>
                    <div class="col my-2">
                        <input type="number" class="form-control" placeholder="TELÉFONO">
                    </div>
                    <div class="col">
                        <input type="text" placeholder="DIRECCIÓN" class="form-control">
                    </div>
                    <div class="col my-2">
                        <textarea placeholder="INFORMACIÓN ADICIONAL" class="form-control" rows="3"></textarea>
                    </div>
                    <div class="col my-4 d-flex justify-content-center">
                        <a class="btn text-white" onclick="confirmDomicilio()" data-dismiss="modal" aria-label="Close" style="background: #38B9C3; border-radius: 20px !important">CONFIRMAR</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- FORM DIGITAL -->
<div class="modal fade" id="exampleModal5" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row-reverse">
                    <div class="col">
                        <label for="" style="color: #38B9C3" class="font-weight-bold lead">CANJEAR PREMIO - DIGITAL</label>
                    </div>
                    <div class="col">
                        <label for="">Por favor registre y confirme los datos para poder canjear y entregar efectivamente tu premio:</label>
                    </div>
                </div>
                <div class="row-reverse my-3">
                    <div class="col">
                        <input type="email" class="form-control" placeholder="CORREO ELECTRÓNICO">
                    </div>
                    <div class="col my-2">
                        <input type="number" class="form-control" placeholder="TELÉFONO">
                    </div>
                    <div class="col my-4 d-flex justify-content-center">
                        <a class="btn text-white" onclick="confirmOnline()" data-dismiss="modal" aria-label="Close" style="background: #38B9C3; border-radius: 20px !important">CONFIRMAR</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- MODAL FELICIDADES -->
<div class="modal fade" id="exampleModal7" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

            </div>
            <div class="modal-body m-0 p-0" style="padding-bottom: 5% !important;">
                <div class="row m-0 p-0">
                    <div class="col-md-3 p-0 m-0 d-flex justify-content-end align-items-center">
                        <div class="list-group fileinput fileinput-new text-center mt-3" data-provides="fileinput">
                            <div class="fileinput-new thumbnail img-circle img-raised p-1 bg-white" style="background: radial-gradient(54.06% 54.06% at 18.84% 26.21%, #AEDC5A 0%, #38B9C3 100%); width: 250px; height:100px">
                                <a href="#" data-toggle="modal" data-target="#exampleModal2"><img src="https://i2.wp.com/hipertextual.com/wp-content/uploads/2014/06/15-cine-scaled.jpg?fit=2560%2C1920&ssl=1" style="width: 100%; height: 100%; border-radius: 60px" rel="nofollow" alt="..."></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8 p-0 m-0">
                        <div class="row-reverse p-0 m-0">
                            <div class="col m-0 p-0">
                                <label for="" class="h3 font-weight-bold" style="float: right; color: #38B9C3">Cine para 2</label>
                            </div>
                            <div class="col m-0 p-0">
                                <hr class="m-0 p-0" style="width: 100%; height: 2px; border-style: solid; border-color: #38B9C3; float: left">
                            </div>
                            <div class="col m-0 p-0">
                                <label for="" class="lead font-weight-bold p-0 m-0" style="float: right; color: #A0C96D">150 Puntos</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row p-0 m-0">
                    <div class="col-4 ml-1">
                        <div class="row-reverse ">
                            <div class="col">
                                <label for="" class="font-weight-bold h4 p-0 m-0" style="color: #A0C96D;"><i class="far fa-check-circle mr-1" style="color: #A0C96D;"></i>CANJEADO</label>
                            </div>
                            <div class="col">
                                <label for="" class="p-0 m-0" style="color: #A0C96D;">2021-11-01</label>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="row-reverse">
                            <div class="col">
                                <label for="" class="font-weight-bold h4" style="color: #38B9C3;">¡FELICIDADES GERMAN LOPÉZ!</label>
                            </div>
                            <div class="col">
                                <label for="" class="h5">Has canjeado 150 puntos es uno de nuestros premios fisicos de Cine.</label>
                            </div>
                            <div class="col my-2">
                                <hr class="m-0 p-0" style="width: 100%; height: 2px; border-style: solid; border-color: #38B9C3; float: left">
                            </div>
                            <div class="col">
                                <label for="">No olvides seguir haciendo los ejericios para acumular más putntos.</label>
                            </div>
                            <div class="col d-flex justify-content-end my-3">
                                <div class="row-reverse">
                                    <div class="col">
                                        <a class="btn text-white" data-dismiss="modal" aria-label="Close" style="background: #AEDC5A; border-radius: 20px !important">SEGUIR CANJEANDO</a>
                                    </div>
                                    <div class="col d-flex justify-content-end">
                                        <label for="" style="text-decoration-line: underline; color: #AEDC5A; ">MI PROGRESO</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script>
    $(document).ready(function() {
        $('.list-group-item').click(function() {
            $('.list-group-item').removeClass('active');
            $(this).closest('.list-group-item').addClass('active')
        });
    });

    function confirmDomicilio() {


        Swal.fire({
            title: 'Estas seguro de canjear el premio?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si'
        }).then((result) => {
            if (result.isConfirmed) {
                Swal.fire(
                    'Canjeando',
                    'El premio ha sido canjeado',
                    'success'
                )
                $('#image').css({
                    'display': 'none'
                })
                $('#image2').css({
                    'display': 'block'
                })
                $('#exampleModal').modal('hide');
                $('#exampleModal7').modal('show');
            }

        })

    }

    function confirmOnline() {


        Swal.fire({
            title: 'Estas seguro de canjear el premio?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si'
        }).then((result) => {
            if (result.isConfirmed) {
                Swal.fire(
                    'Canjeando',
                    'El premio ha sido canjeado',
                    'success'
                )
                $('#imageO1').css({
                    'display': 'none'
                })
                $('#imageO2').css({
                    'display': 'block'
                })
                $('#exampleModal6').modal('hide');
                $('#exampleModal7').modal('show');
            }

        })

    }
</script>